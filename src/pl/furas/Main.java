package pl.furas;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;

/**
 * Główna klasa uruchamiajaca grę
 * 
 * @author furas
 */
public class Main extends JFrame {

    /**
     * Plansza gry
     */
    private Plansza plansza;
       
    private JMenuBar menubar;
        
    private JMenu menuGra;
    private JMenuItem menuGraPolacz;
    private JMenuItem menuGraRozlacz;
    private JMenuItem menuGraUstawienia;
    private JPopupMenu.Separator menuGraSeparator;
    private JMenuItem menuGraZakoncz;

    private JMenu menuPomoc;
    private JMenuItem menuPomocOProgramie;
        
    private String host;
    private int port;
    
    
    /**
     * Gra w weze
     * 
     * @param host adres serwera
     * @param port port serwera
     */
    public Main(String host, int port) {
        super();

        this.host = host;        
        this.port = port;
        
        //setDefaultCloseOperation(EXIT_ON_CLOSE);
        setTitle("Węże");
        
        initMenu();
        
        setLayout(new BorderLayout());

        plansza = new Plansza();

        plansza.setVisible(true);
        plansza.setPreferredSize(new Dimension(641, 481));
        
        plansza.setFocusable(true);
        plansza.requestFocusInWindow();        

        add(plansza, BorderLayout.CENTER);

        pack();
        setVisible(true);
        setResizable(false);
        
        addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
                zamykanie();                
            }
        });
    }

    /**
     * Tworzenie menu 
     */
    private void initMenu() {
        menubar = new JMenuBar();

        //--- menu GRA ---
        
        menuGra = new JMenu("Gra");
        
        //---
        
        menuGraPolacz = new JMenuItem("Połącz");
        menuGraPolacz.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_MASK));
        menuGraPolacz.setMnemonic('P');
        menuGraPolacz.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGraPolaczActionPerformed(evt);
            }
        });
        menuGra.add(menuGraPolacz);
        
        //---
        
        menuGraRozlacz = new JMenuItem("Rozłącz");
        menuGraRozlacz.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.CTRL_MASK));
        menuGraRozlacz.setMnemonic('R');
        menuGraRozlacz.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGraRozlaczActionPerformed(evt);
            }
        });
        menuGraRozlacz.setEnabled(false);
        menuGra.add(menuGraRozlacz);
        
        //---
        
        menuGraUstawienia = new JMenuItem("Ustawienia...");
        menuGraUstawienia.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_U, java.awt.event.InputEvent.CTRL_MASK));
        menuGraUstawienia.setMnemonic('U');
        menuGraUstawienia.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGraUstawieniaActionPerformed(evt);
            }
        });        
        menuGra.add(menuGraUstawienia);
        
        //---
        
        menuGraSeparator = new JPopupMenu.Separator();
        menuGra.add(menuGraSeparator);
        
        //---
        
        menuGraZakoncz = new JMenuItem("Zakończ");
        
        menuGraZakoncz.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_MASK));
        menuGraZakoncz.setMnemonic('k');
        menuGraZakoncz.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuGraZakonczActionPerformed(evt);
            }
        });
        menuGra.add(menuGraZakoncz);

        //--- menu POMOC ---
        
        menuPomoc = new JMenu("Pomoc");

        //--
        
        menuPomocOProgramie = new JMenuItem("O Programie");
        menuPomocOProgramie.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPomocOProgramieActionPerformed(evt);
            }
        });
        menuPomoc.add(menuPomocOProgramie);

        //---
        
        menubar.add(menuGra);
        menubar.add(menuPomoc);

        //---
        
        setJMenuBar(menubar);
    }

    /**
     * Obsługa zdarzenia "Połącz" z menu "Gra"
     * 
     * @param evt zdarzenie
     */
    private void menuGraPolaczActionPerformed(java.awt.event.ActionEvent evt) {
        System.out.println("Połączenie");
        
        if( plansza.polacz(host, port) && plansza.czyWatekZyje() ) {
            menuGraPolacz.setEnabled(false);
            menuGraRozlacz.setEnabled(true);
        } else {
            JOptionPane.showMessageDialog(this, "Problem z połączeniem");
        }
    }         
    
    /**
     * Obsługa zdarzenia "Rozłącz" z menu "Gra"
     * 
     * @param evt zdarzenie
     */
    private void menuGraRozlaczActionPerformed(java.awt.event.ActionEvent evt) {
        System.out.println("Rozłączenie");
        
        plansza.rozlacz();
        
        menuGraPolacz.setEnabled(true);
        menuGraRozlacz.setEnabled(false);
    }         

    /**
     * Obsługa zdarzenia "Ustawienia" z menu "Gra" - zmiana ustawień hosta i portu
     * 
     * @param evt zdarzenie
     */
    private void menuGraUstawieniaActionPerformed(java.awt.event.ActionEvent evt) {
        
        String wynik = JOptionPane.showInputDialog(this, "Host i port (domyślny port: 5000): ", this.host + " " + this.port);
        
        if( wynik != null && wynik.length() > 0 ) {
            String args[] = wynik.split(" ");
            this.host = args[0];
            if( args.length > 1 ) {
                this.port = Integer.parseInt(args[1]);
            } else {
                this.port = 5000; // domyślny port
            }
        }
        
        System.out.println("Ustawienia: " + this.host + " " + this.port);
    }         

    /**
     * Obsługa zdarzenia "Zakończ" z menu "Gra" - zamknięcie programu
     * 
     * @param evt zdarzenie
     */
    private void menuGraZakonczActionPerformed(java.awt.event.ActionEvent evt) {
        zamykanie();
    }         

    
    /**
     * Obsługa zdarzenia "Zakończ" z menu "Gra" - zamknięcie programu
     * 
     * @param evt zdarzenie
     */
    private void menuPomocOProgramieActionPerformed(java.awt.event.ActionEvent evt) {                                                    
        JOptionPane.showMessageDialog(this, "Weżę 0.1\n\nAutor: Bartłomiej 'furas' Burek\nEmail: furas@mat.umk.pl", "O Programie", JOptionPane.INFORMATION_MESSAGE);
    }     
    
    private void zamykanie() {
        System.out.println("Zamykanie");
        plansza.rozlacz();
        dispose();
    }
    
    /**
     * Uruchamianie programu 
     * 
     * @param args argumenty przekazane do programu - host i port
     */
    public static void main(String[] args) {
        // wartosci domyślne
        String _host = "localhost";
        int _port = 5000;
        
        // wartości przekazane w parametrach
        if( args.length > 0) _host = args[0];
        if( args.length > 1) _port = Integer.parseInt(args[1]);
        
        new Main(_host, _port);
    }    
}
