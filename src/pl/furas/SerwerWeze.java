package pl.furas;

import java.net.ServerSocket;
import java.net.Socket;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.IOException;

import java.awt.Point;
import java.util.LinkedList;
import java.util.HashMap;
import java.util.Map;

/**
 * Serwer gry
 * @author furas
 */
public class SerwerWeze {

    private static int MAX_GRACZE = 2;
    
    private ServerSocket gniazdoSerwer;
    private final int port;
    
    // lista z polozeniem graczy
    HashMap<Integer, LinkedList<Point>> gracze;
    
//    private final String wersja = "0.0.1";

    /**
     * Serwer gry w węże
     * @param port port nasłuchu serwera
     */
    private SerwerWeze(int port) {
        this.port = port;
        this.gracze = new HashMap<>();
    }

    /**
     * Wyswietlanie komunikatów
     * @param komunikat komunikat do wyswietlenia
     */
    private void log(String komunikat) {
        System.out.println("Serwer: " + komunikat);
    }
    
    /**
     * Uruchamianie serwera
     */
    private void start() {
        log("Startuje ...");

        try {
            gniazdoSerwer = new ServerSocket(this.port);
            log("start: otwarte: " + gniazdoSerwer.toString());
            
            while( true ) {
                SerwerWatek watek = new SerwerWatek(gniazdoSerwer.accept());
                watek.start();
            }            
        } catch(IOException ex) {
            log("start: IOException: " + ex);
        }
    }

    /**
     * Zatrzymywanie serwera
     */
    private void stop() {
        try {
            log("stop: zamkniete: " + gniazdoSerwer.toString());
            gniazdoSerwer.close();
        } catch (IOException ex) {
            log("stop: IOException: " + ex);
        }
        
        log("Zatrzymuje ...");
    }

    //-------------------------------------------------------------------------
    /**
     * Watek do obsługi pojedynczego gracza
     */
    class SerwerWatek extends Thread {
    
        private final Socket gniazdo;
        private final int port;
        private Boolean czyPolaczony; // czy gracz podlaczony do serwera
                
        private ObjectInputStream czytaczObiektow;
        private ObjectOutputStream pisaczObiektow;
    
        private Boolean czyGra; // czy gracz w trakcie gry

        /**
         * Konstruktor 
         * @param gniazdo gniazdo do komunikacji z klientem
         */
        public SerwerWatek(Socket gniazdo) {
            this.gniazdo = gniazdo;
            this.port = gniazdo.getPort();
            this.czyPolaczony = true;
            this.czyGra = false;
            
            log("otwarte: " + this.gniazdo);
        }

        /**
         * Wypisywanie komunikatu 
         * @param komunikat komunikat do wypisania
         */
        private void log(String komunikat) {
            System.out.println("Serwer Weze [" + port + "]: " + komunikat);
        }
        
        /**
         * Wysylanie pojedynczej wiadomosci
         * @param wiadomosc wiadomość do wysłania
         */
        private void wyslij(String wiadomosc) {
            try {
                pisaczObiektow.writeUTF(wiadomosc);
                pisaczObiektow.flush();
                log(wiadomosc);
            } catch (IOException ex) {
                log("wyslij: IOException: " + ex);
            }
        }
        
        /**
         * Główna funkcja wątku
         */
        @Override
        public void run() {
            try {
                czytaczObiektow = new ObjectInputStream(this.gniazdo.getInputStream());
                pisaczObiektow = new ObjectOutputStream(this.gniazdo.getOutputStream());
                pisaczObiektow.flush();                
                
                String linia;
                
                while( czyPolaczony ) {
                    
                    linia = czytaczObiektow.readUTF();
                    
                    if( linia == null ) {
                        log("run: ERROR: linia == null");
                        czyPolaczony = false;
                    } else {
                        log("run: <-- " + linia);
                        
                        String args[] = linia.split(" ");
                        String komenda = args[0].toUpperCase();

                        switch(komenda) {
                            case "POMOC":
                                komendaPomoc(args); break;                                
                            case "WERSJA":
                                komendaWersja(args); break;                                
                            
                            case "GRANIE": // dolaczenie do gry
                                komendaGranie(args); break;                                
                            case "PRZERWANIE": // przerwanie gry
                                komendaPrzerwanie(args); break;                                

                            case "GRACZ": // odebranie pozycji gracz
                                komendaGracz(args); break;                                
                            case "PRZECIWNIK": // przeslanie pozycji przeciwwnikow
                                komendaPrzeciwnik(args); break;                                

                            default:
                                log("run: ERROR: nieznana komenda !");
                                wyslij("ERROR nieznana komenda");
                        }
                    } 
                }
                
                // dla pewnosci usuwam gracza gdyby nie było to zrobione poprawnie
                synchronized(gracze) {
                    gracze.remove(port);
                }
                
                pisaczObiektow.close();
                czytaczObiektow.close();
                
                log("run: zamykniete:" + this.gniazdo);
                gniazdo.close();
                
            } catch (IOException ex) {
                log("run: IOException: " + ex);
            }
            
        }
        
        /**
         * Obsługa komendy "ROZŁACZANIE"
         * 
         * @param args dodatkowe parametry przekazane w komendzie
         */
        private void komendaRozlaczenie(String[] args) {
            czyPolaczony = false;
            wyslij("OK 0 tak szybko ???");
            log("komendaRozlaczenie: OK");
        }
        
        /**
         * Obsługa komendy "POMOC" - zwrócenie komunikatu pomocy
         * 
         * @param args dodatkowe parametry przekazane w komendzie 
         */
        private void komendaPomoc(String[] args) {
            wyslij("OK 0 nic z tego :)");
            log("komendaPomoc: OK");
        }
        
        /**
         * Obsługa komendy "WERSJA" - zwrócenie wersji serwera
         * 
         * @param args dodatkowe parametry przekazane w komendzie 
         */
        private void komendaWersja(String[] args) {
            wyslij("OK 0 jak zawsze najnowsza :)");
            log("komendaWersja: OK");
        }
        
        /**
         * Obsługa komendy "GRANIE" - dołączenie gracza do gry
         * 
         * @param args dodatkowe parametry przekazane w komendzie 
         */
        private void komendaGranie(String[] args) {
            synchronized(gracze) {
                if( gracze.containsKey(port) || gracze.size() < MAX_GRACZE ) {
                    gracze.put(port, new LinkedList<Point>());
                    log("komendaGranie: <-- " + gracze.get(port));

                    wyslij("OK 0");
                    log("komendaGranie: OK");            
                    czyGra = true;
                } else {
                    wyslij("komendaGranie: --> ERROR 0 serwer pelny");
                }                 
            }
        }
        
        /**
         * Obsługa komendy "PRZERWANIE" - zrezygnowanie gracza z gry
         * 
         * @param args dodatkowe parametry przekazane w komendzie 
         */
        private void komendaPrzerwanie(String[] args) {
            synchronized(gracze) {
                gracze.remove(port);
            }
            czyGra = false;
            wyslij("OK 0");
            log("komendaPrzerwanie: OK");            
        }
        
        /**
         * Obsługa komendy "GRACZ" - pobieranie pozycji gracza
         * 
         * @param args dodatkowe parametry przekazane w komendzie 
         */
        private void komendaGracz(String[] args) {
            if( czyGra == false ) {
                wyslij("ERROR 0");
            } else {
                Integer liczba = Integer.parseInt(args[1]);
                
                if( liczba == 1 ) {
                    try {
                        synchronized(gracze) {
                            if( gracze.containsKey(port) ) {
                                gracze.put(port, (LinkedList<Point>)this.czytaczObiektow.readObject());
                                log("komendaGracz: <-- " + gracze.get(port));
                                wyslij("OK 0");
                            } else {
                                wyslij("komendaGracz: --> ERROR 0 serwer pelny");
                            } 
                        }
                    } catch (IOException ex) {
                        log("problem: IOException: " + ex);
                    } catch (ClassNotFoundException ex) {
                        log("problem: ClassNotFoundException: " + ex);
                    }
                } else {
                    wyslij("komendaGracz: --> ERROR 0");
                }
            }
        }
        
        /**
         * Obsługa komendy "PRZECIWNIK" - wysyłanie pozycji przeciwnika lub null
         * 
         * @param args dodatkowe parametry przekazane w komendzie 
         */
        private void komendaPrzeciwnik(String[] args) {
            if( czyGra == false ) {
                wyslij("ERROR 0");
            } else {
                synchronized(gracze) {
                    if( gracze.size()== 1) {
                        wyslij("OK 0");
                    } else {

                        wyslij("OK " + (gracze.size()-1) );

                        for(Map.Entry el: gracze.entrySet()) {
                            if( (Integer)el.getKey() != gniazdo.getPort() ) {
                                try {
                                    this.pisaczObiektow.writeObject(el.getValue());
                                    this.pisaczObiektow.flush();
                                    log("komendaPrzeciwnik: --> " + el.getValue());
                                } catch (IOException ex) {
                                    log("komendaPrzeciwnik: IOException: " + ex);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Startowanie serwera
     * 
     * @param args arg[0] numer portu do nasluchu serwera
     */
    public static void main(String[] args) {

        int port = 5000;
        
        if( args.length > 0 ) port = Integer.parseInt(args[0]);
        
        SerwerWeze serwer = new SerwerWeze(port);
        serwer.start();        
    }
}
