package pl.furas;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

/**
 * Wąż pojawiający się w grze
 * @author furas
 */
public class Waz {

    /**
     * Stałe określające kierunek ruchu węża
     */
    public static int PRAWO = 1;
    public static int DOL   = 2;
    public static int LEWO  = 3;
    public static int GORA  = 4;
    public static int SIZE  = 16;
    
    private static int MIN_X = 0; 
    private static int MAX_X = 39;
    private static int MIN_Y = 0; 
    private static int MAX_Y = 29;

    /**
     * Stała określająca początkową długość węża
     */
    public static int DLUGOSC = 5;

    //------------------------------------------------------------------------

    /**
     * Aktualna długość węża (ogon + glowa)
     */
    private int iloscSegmentow;
    
    public void zwiekszIloscSegmentow() {
        iloscSegmentow += 2;
    }
    /**
     * Pozycja segmentów węża (x,y) liczona w kratkach (a nie pikselach ekranu)
     */
    private LinkedList<Point> segmenty; 
    
    /**
     * Kierunek ruchu 
     */
    private int kierunek;
    
    /**
     * Kolory ogona 
     */
    private Color kolorOgona;
    
    /**
     * Kolor głowy 
     */
    private Color kolorGlowy;
    
    //------------------------------------------------------------------------

    /**
     * Pozycja głowy w kratkach
     * 
     * @return pozycja głowy (Point) 
     */
    public Point getPozycjaGlowy() {
        return this.segmenty.getFirst();
    }
    
    /**
     * Ustawianie segmentów węża
     * 
     * @param segmenty lista segmentów (Point)
     */
    public void setSegmenty(LinkedList<Point> segmenty) {
        this.segmenty = segmenty;
    }
    
    /**
     * Pobranie segmentów węża
     * 
     * @return lista segmentów
     */
    public LinkedList<Point> getSegmenty() {
        return this.segmenty;
    }
    
    //------------------------------------------------------------------------
    
    /**
     * Konstruktor z domyślnym kolorem głowy
     * 
     * @param kolor_ogona kolor ogona
     */
    public Waz(Color kolor_ogona) {
        this(kolor_ogona, Color.RED);
    }
    
    /**
     * Kontruktor 
     * 
     * @param kolor_ogona kolor ogona
     * @param kolor_glowy kolor glowy
     */
    public Waz(Color kolor_ogona, Color kolor_glowy) {

        this.kolorGlowy = kolor_glowy;
        this.kolorOgona = kolor_ogona;
        
        init();
    }

    /**
     * inicjalizacja danych na poczatek gry 
     * lub reinicjalizacja danych gdy dojdzie do kolizji
     */
    private void init() {

        this.iloscSegmentow = Waz.DLUGOSC;
        
        this.segmenty = new LinkedList<Point>();
        
        Random r = new Random();

        int _x = DLUGOSC + r.nextInt(MAX_X - 2*DLUGOSC);
        int _y = DLUGOSC + r.nextInt(MAX_Y - 2*DLUGOSC);

        for(int i = 0; i < this.iloscSegmentow ; i++) {
            this.segmenty.add(new Point(_x+i, _y));
        }

        this.kierunek = Waz.LEWO;
    }

    /**
     * Rysowanie węża
     * 
     * @param g "grafika"
     */
    public void draw(Graphics2D g) {

        Iterator<Point> iter = segmenty.iterator();
        
        // glowa
        
        drawSegment(g, iter.next(), kolorGlowy);

        // ogon
        
        while( iter.hasNext() ) {
            drawSegment(g, iter.next(), kolorOgona);
        }
    }

    /**
     * Rysowanie pojedynczego segmentu 
     * 
     * @param g "grafika"
     * @param pozycja pozycja segmentu w kratkach 
     * @param kolor kolor wypełnienia
     */
    private void drawSegment(Graphics2D g, Point pozycja, Color kolor) {
        int x = pozycja.x * Waz.SIZE;
        int y = pozycja.y * Waz.SIZE;

        g.setColor(kolor);
        g.fillRect(x, y, Waz.SIZE, Waz.SIZE);
        g.setColor(Color.black);
        g.drawRect(x, y, Waz.SIZE, Waz.SIZE);
    }
    
    /**
     * Aktualizacja pozycji węża w zależności od kierunku ruchu 
     * i napotkanej ściany zewnętrznej
     */
    public void update() {
        
        Boolean zmiana = false;

        Point glowa = new Point(this.segmenty.getFirst());
        
        if( this.kierunek == Waz.PRAWO ) {
            if( glowa.x < MAX_X ) { // sciana zewnętrzna
                glowa.x += 1;
                zmiana = true;
            }
        } else if( this.kierunek == Waz.LEWO ) {
            if( glowa.x > MIN_X ) { // sciana zewnętrzna
                glowa.x -= 1;
                zmiana = true;
            }
        } else if( this.kierunek == Waz.DOL ) {
            if( glowa.y < MAX_Y) { // sciana zewnętrzna
                glowa.y += 1;
                zmiana = true;
            }
        } else if( this.kierunek == Waz.GORA ) {
            if( glowa.y > MIN_Y ) { // sciana zewnętrzna
                glowa.y -= 1;
                zmiana = true;
            }
        }
        
        // dodawanie nowego segmentu z glową 
        // i usuniecie ostatniego segmentu ogona
        if( zmiana ) {
            // usuwanie konca ogonu jesli nie ma sie wydluzyc
            if( iloscSegmentow == this.segmenty.size() )
                this.segmenty.pollLast();

            // glowa w nowym miejscu
            this.segmenty.addFirst(glowa);
        }
    }

    //------------------------------------------------------------------------
    
    /**
     * Obsługa klawiszy do zmiany kierunku węża
     * 
     * @param e zdarzenie
     */
    public void keyPressed(KeyEvent e) {
        
        int klawisz = e.getKeyCode();
        
        if( klawisz == KeyEvent.VK_UP ) {
            if( this.kierunek != DOL )
                this.kierunek = GORA;
        } else if( klawisz == KeyEvent.VK_DOWN ) {
            if( this.kierunek != GORA )
                this.kierunek = DOL;
        } else if( klawisz == KeyEvent.VK_LEFT ) {
            if( this.kierunek != PRAWO )
                this.kierunek = LEWO;
        } else if( klawisz == KeyEvent.VK_RIGHT ) {
            if( this.kierunek != LEWO )
                this.kierunek = PRAWO;
        }
        
    }    
}
