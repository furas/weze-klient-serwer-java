package pl.furas;

import java.net.Socket;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.IOException;
import java.awt.Point;
import java.util.LinkedList;

/**
 * Klasa odpowiadająca za połączenie z serwerem i przesyłanie danych
 * 
 * @author furas
 */
public class KlientWeze {
    
    /**
     * Dane serwera - adres i port
     */
    private String host;
    private int port;
    
    /**
     * Połączenie z serwerem - gniazdo i strumienie odczytu i zapisu obiektów
     */
    private Socket gniazdo;
    private ObjectInputStream czytaczObiektow;
    private ObjectOutputStream pisaczObiektow;

    //-------------------------------------------------------------------------
    
    private Boolean polaczony;
    private String odpowiedz;
    
    /**
     * Czy jest połączenie z serwerem
     * 
     * @return true/false
     */
    public Boolean czyPolaczony() { return this.polaczony; }

    /**
     * Tresc ostatniej odpowiedzi
     * 
     * @return odpowiedz z serwera na wyslaną komendę
     */
    public String getOdpowiedz() { return this.odpowiedz; }

    /**
     * Konstruktor 
     * 
     * @param host adres serwera
     * @param port port serwera
     */
    KlientWeze(String host, int port) {
        this.host = host;
        this.port = port;
        this.polaczony = false;
    }

    /**
     * Tworzenie połączenia z serwerem
     * 
     * @return true/false czy udalo sie polaczyc
     */
    public Boolean start() {
        log("Startuje ...");

        try {
            this.gniazdo = new Socket(this.host, this.port);
            log("gniazdo: otwarte: " + this.gniazdo.toString());
            
            this.pisaczObiektow  = new ObjectOutputStream(this.gniazdo.getOutputStream());
            this.pisaczObiektow.flush();
            log("pisaczObiektow: otwarty");

            this.czytaczObiektow = new ObjectInputStream(this.gniazdo.getInputStream());
            log("czytaczObiektow: otwarty");

            this.polaczony = true;
            log("polaczony");                      
        } catch (IOException ex) {
            log("start: IOException:" + ex);
            return false;
        }
        
        return true;
    }

    /**
     * Konczenie połączenia z serwerem
     * 
     * @return true/false czy udalo sie polaczyc
     */
    public Boolean stop() {
        try {
            polaczony = false;
            log("rozlaczony");
            
            this.czytaczObiektow.close();
            log("pisaczObiektow: zamkniety");
            
            this.pisaczObiektow.close();
            log("czytaczObiektow: zamkniety");

            String gniazdoString = this.gniazdo.toString();
            this.gniazdo.close();
            log("gniazdo: zamkniete: " + gniazdoString);
            
        } catch (IOException ex) {
            log("stop: IOException:" + ex);
            return false;
        }
        
        return true;
    }

    /**
     * Wysyłanie komendy do serwera z domyslną czekaniem na odpowiedz
     * 
     * @param komenda komenda wysylana do serwera
     * @return odpowiedz z serwera lub null
     */
    public String komenda(String komenda) {
        odpowiedz = this.komenda(komenda, true);
        return odpowiedz;
    }

    /**
     * Wysyłanie komendy do serwera 
     * 
     * @param komenda komenda wysylana do serwera
     * @param czekacNaOdpowiedz czy czekac na odpowiedz serwera
     * @return odpowiedz z serwera lub null
     */
    public String komenda(String komenda, Boolean czekacNaOdpowiedz) {
        odpowiedz = null;

        try {
            this.pisaczObiektow.reset();
            this.pisaczObiektow.writeUTF(komenda);
            this.pisaczObiektow.flush();
            log("sendKomenda: --> " + komenda);
            
            if( czekacNaOdpowiedz ) {
                odpowiedz = this.czytaczObiektow.readUTF();
                log("sendKomenda: <-- " + odpowiedz);
            }
        } catch (IOException ex) {
            log("sendKomenda: IOException: " + ex);
            odpowiedz = null;
        }
        
        return odpowiedz;
    }
    
    /**
     * Komenda "GRANIE" - dołączenie do gry
     * 
     * @return true/false czy gra rozpoczeta
     */
    public Boolean komendaGranie() {
        odpowiedz = komenda("GRANIE");

        return odpowiedz != null && odpowiedz.equals("OK 0");
    }
    
    /**
     * Komenda "PRZERWANIE - rezygnacja z gry
     * 
     * @return odpowiedz z serwera lub null 
     */
    public Boolean komendaPrzerwanie() {
        odpowiedz = komenda("PRZERWANIE");

        return odpowiedz != null && odpowiedz.equals("OK 0");
    }
    
    /**
     * Komenda "GRACZ" - wysłanie pozycji gracza
     * 
     * @param segmenty lista pozycji segmentów gracza
     * @return odpowiedz z serwera lub null  
     */
    public String komendaGracz(LinkedList<Point> segmenty) {
        try {
            this.pisaczObiektow.reset();
            this.pisaczObiektow.writeUTF("GRACZ 1");
            this.pisaczObiektow.writeObject(segmenty);
            this.pisaczObiektow.flush();
            log("sendGracz: --> GRACZ 1 " + segmenty);
            
            odpowiedz = this.czytaczObiektow.readUTF();
            log("sendGracz: <-- " + odpowiedz);
        } catch (IOException ex) {
            log("sendGracz: IOException: " + ex);
            odpowiedz = null;
        }
        
        return odpowiedz;
    }

    /**
     * Komenda "PRZECIWNIK" - odbieranie pozycji przeciwnika
     * 
     * @return pozycja przeciwnika lub null
     */
    public LinkedList<Point> komendaPrzeciwnik() {
        LinkedList<Point> przeciwnik = null;
        
        try {
            this.pisaczObiektow.writeUTF("PRZECIWNIK");
            this.pisaczObiektow.flush();
            log("recvPrzeciwnik: --> PRZECIWNIK");
            
            odpowiedz = this.czytaczObiektow.readUTF();
            log("recvPrzeciwnik: <-- " + odpowiedz);
            
            if( odpowiedz.equals("OK 1") ) {
                przeciwnik = (LinkedList<Point>)this.czytaczObiektow.readObject();
                log("recvPrzeciwnik: <-- " + przeciwnik);
            }
        } catch (IOException ex) {
            log("recvPrzeciwnik: IOException: " + ex);
            przeciwnik = null;
        } catch (ClassNotFoundException ex) {
            log("recvPrzeciwnik: ClassNotFoundException: " + ex);
            przeciwnik = null;
        }
        
        return przeciwnik;
    }

    /**
     * Wypisywanie komunikatu
     * 
     * @param komunikat komunikat do wypisania
     */
    private void log(String komunikat) {
        System.out.println("Klient Weze: " + komunikat);
    }
    
    /**
     * funkcja main() aby mozna bylo testowac samo polaczenie z serwerem
     * 
     * @param args argumenty przekazywane w linii komend
     */
    public static void main(String[] args) {
        KlientWeze klient = new KlientWeze("127.0.0.1", 5000);
        
        klient.start();
        klient.komenda("WERSJA");
        klient.komenda("POMOC");
        klient.stop();
    }    

}
