/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.furas;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;

/**
 *
 * @author furas
 */
public class Jablko {
    
    private Point pozycja;
    private Color kolor;

    public Jablko(Color kolor) {
        this.kolor = kolor;
    }
    

    public void setPozycja(Point pozycja) {
        this.pozycja = pozycja;
    }
    
    public void setPozycja(int x, int y) {
        this.pozycja = new Point(x,y);
    }
    
    public Point getPozycja() {
        return this.pozycja;
    }
    
    public void draw(Graphics2D g) {
        int x = pozycja.x * Waz.SIZE;
        int y = pozycja.y * Waz.SIZE;

        g.setColor(kolor);
        g.fillRect(x, y, Waz.SIZE, Waz.SIZE);
        g.setColor(Color.black);
        g.drawRect(x, y, Waz.SIZE, Waz.SIZE);
    }
}
