
package pl.furas;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.LinkedList;
import java.util.Random;
import javax.swing.JPanel;

/**
 * Widget na którym odbywa się gra
 * @author furas
 */
public class Plansza extends JPanel implements KeyListener {

    /**
     * Ilość klatek na sekundę
     */
    private final int FPS = 5;
    
    /**
     * Pozycja gracza i przeciwnika
     */
    private Waz gracz;
    private Waz przeciwnik;
    private Jablko jablko;
    
    private Boolean czyJablko;
    
    /**
     * Różne stany podczas działania programu
     */
    private Boolean czyGranie; // czy połączono się z serwerem i rozpoczęto grę
    private Boolean czyPrzeciwnik; // czy jest dostępny przeciwnik w grze
    private Boolean czyKoniecGry; // czy gra się zakończyła - czyli GAME OVER

    private Boolean czyPetla;
    
    private String wynik; // napis na koniec gry (gdy GAME OVER)
    
    private KlientWeze klient; // klasa zajmująca się połaczeniem z serwerem
    
    private Thread watekMainLoop; // wątek głównej pętli gry
    
    private Random r;
    //-------------------------------------------------------------------------

    public Boolean czyWatekZyje() { return watekMainLoop != null && watekMainLoop.isAlive(); }
    
    //-------------------------------------------------------------------------
    
    public Plansza() {
        super();

        setDoubleBuffered(true);
        addKeyListener((KeyListener)this);
        r = new Random();
        
        czyGranie = false;        
    }
    
    /**
     * Łaczenie się z serwerem
     * 
     * @param host adres serwera
     * @param port port serwera
     * @return true/false czy udało się połączyć i utworzyć wątek
     */
    public Boolean polacz(final String host, final int port) {
        log("polacz: " + host + " " + port);
        
        // polaczenie z serwerem
        klient = new KlientWeze(host, port);
        
        if( !klient.start() ) {
            return false;
        }

        if( !klient.komendaGranie() ) {
            klient.stop();
            return false;
        }

        czyGranie = true;
        
        utworzWatek();
        
        return czyWatekZyje();
    }

    /**
     * Rozłączanie się z serwerem
     */
    public void rozlacz() {

        try {
            // przerwanie wątku
            if( czyWatekZyje() ) {
                czyPetla = false;
                watekMainLoop.join();
            }
        } catch (InterruptedException ex) {
            log("zakonczenie: InterruptedException: " + ex);
        }
        
        // przerwanie gry z serwerem
        if( czyGranie ) {
            klient.komendaPrzerwanie();
            czyGranie = false;
        }
        
        // rozlaczenie z serwerem
        if( klient != null && klient.czyPolaczony() ) {
            klient.stop();
        }
        
        log("rozlacz");
        
        repaint();
    }
    
    /**
     * Wypisywanie komunkatów
     * 
     * @param komunikat komunikat do wypisania
     */
    private void log(String komunikat) {
        System.out.println("Klient Plansza: " + komunikat);
    }
    
    /**
     * Utworzenie wątku do obsługi głównej pętli gry
     */
    private void utworzWatek() {
        
        watekMainLoop = new Thread() {
            @Override
            public void run() {
                gracz = new Waz(Color.GREEN);
                przeciwnik = new Waz(Color.ORANGE);
                
                czyPrzeciwnik = false;
                czyKoniecGry = false;

                jablko = new Jablko(Color.BLUE);
                losujJablko();
                czyJablko = true;
                
                wynik = "";
                         
                // glowna petla gry
                mainLoop();

            }
        };
        
        // start watku
        watekMainLoop.start();
    }
    
    /**
     * Rysowanie na planszy różnych rzeczy w zależności od stanu gry
     * 
     * @param g "grafika"
     */
    @Override
    public void paint(Graphics g) {
        super.paint(g); 
        
        log("czyGranie: " + czyGranie);
        
        if( czyGranie ) {
            gracz.draw((Graphics2D)g);

            if( czyPrzeciwnik ) {
                przeciwnik.draw((Graphics2D)g);
            }

            if( czyJablko ) {
                jablko.draw((Graphics2D)g);
            }
            
            if( czyKoniecGry ) {
                int dlugosc  = (int) ((Graphics2D)g).getFontMetrics().getStringBounds(wynik, ((Graphics2D)g)).getWidth();  
                int wysokosc = (int) ((Graphics2D)g).getFontMetrics().getStringBounds(wynik, ((Graphics2D)g)).getHeight();  

                int x = (640 - dlugosc)/2;  
                int y = (480 - wysokosc)/2;  
                g.drawString(wynik, x, y);

                System.out.println("napis wymiary: " + dlugosc + "x" + wysokosc);
            }
        } else {
            String tytul = "WĘŻĘ";
            
            int dlugosc  = (int) ((Graphics2D)g).getFontMetrics().getStringBounds(tytul, ((Graphics2D)g)).getWidth();  
            int wysokosc = (int) ((Graphics2D)g).getFontMetrics().getStringBounds(tytul, ((Graphics2D)g)).getHeight();  

            int x = (640 - dlugosc)/2;  
            int y = (480 - wysokosc)/2;  
            g.drawString(tytul, x, y);
        }
    }
    
    /** 
     * Główna pętla gry - zmiana pozycji graczy, sprawdzanie kolizji, 
     * wypisywanie komunikatów o zakończeniu gry
     */
    private void mainLoop() {
        
        log("mainLoop: start");

        czyKoniecGry = false;
        czyPrzeciwnik = false;
        czyPetla = true;
        
        while(czyPetla) {
            
            // --- updates ---
            
            if( !czyKoniecGry ) {
                updateGracz();
                updatePrzeciwnik();
            
                // --- collision detection ---
                
                if( czyPrzeciwnik ) {
                    Boolean graczZlapal = checkGracz();
                    Boolean przeciwnikZlapal = checkPrzeciwnik();

                    if( graczZlapal && przeciwnikZlapal ) {
                        log("REMIS");
                        czyKoniecGry = true;
                        wynik = "REMIS";
                    } else if( graczZlapal ) {
                        log("Wygrales");
                        czyKoniecGry = true;
                        wynik = "Wygrałeś";
                    } else if( przeciwnikZlapal ) {
                        log("Przegrales");
                        czyKoniecGry = true;
                        wynik = "Przegrałeś";
                    }
                }
            }
            
            // --- draws ---
            
            repaint();
            
            // --- FPS ---
            
            try {
                Thread.sleep(1000/FPS);
            } catch (InterruptedException ex) {
                log("mainLoop: Thread.sleep: \n" + ex);
            }
        }
        
        czyPetla = false;

        System.out.println("mainLoop: stop");
    }

    /**
     * Sprawdzenie czy gracz ugryzł przeciwnika
     * @return true jesli gracz ugryzł przeciwnika
     */
    private Boolean checkGracz() {
        if( klient.czyPolaczony() && czyPrzeciwnik ) {
            return przeciwnik.getSegmenty().contains(gracz.getPozycjaGlowy());
        }
        return false;
    }

    /**
     * Sprawdzanie czy przeciwnik ugryzł gracza
     * @return true jesli przeciwnik ugryzł gracza
     */
    private Boolean checkPrzeciwnik() {
        if( klient.czyPolaczony() && czyPrzeciwnik ) {
            return gracz.getSegmenty().contains(przeciwnik.getPozycjaGlowy());
        }
        
        return false;
    }

    /**
     * Aktualizacja pozycji gracza
     * i wysłanie pozycji na serwer
     */
    private void updateGracz() {
        if( !czyKoniecGry ) {
            gracz.update();
            log("sprawdzanie pozycji " + gracz.getPozycjaGlowy() + " " + jablko.getPozycja()); 
            if( gracz.getPozycjaGlowy().equals(jablko.getPozycja()) ) {
                log("wydluzanie weza"); 
                gracz.zwiekszIloscSegmentow();
                losujJablko();              
            }
        }
        
        if( klient.czyPolaczony() ) {
            klient.komendaGracz(gracz.getSegmenty());
        }
    }
    
    /**
     * Odebranie pozycji przeciwnika z serwera
     * i aktualizacja pozycji przeciwnika 
     */
    private void updatePrzeciwnik() {
        if( klient.czyPolaczony() && czyGranie ) {
            LinkedList<Point> lista = klient.komendaPrzeciwnik();
            czyPrzeciwnik = (lista != null && lista.size() != 0);
            if( czyPrzeciwnik ) {
                przeciwnik.setSegmenty(lista);
                log("nowa pozycja przeciwnika: " + lista);
            }
        }
    }
    
    /**
     * Obsługa klawiszy 
     * @param e zdarzenie 
     */
    @Override
    public void keyPressed(KeyEvent e) {
        if( gracz != null ) {
            gracz.keyPressed(e);
        }
    }
    
    @Override
    public void keyTyped(KeyEvent e) {}

    @Override
    public void keyReleased(KeyEvent e) {}

    private void losujJablko() {
        Point pozycja = new Point(r.nextInt(39), r.nextInt(29));
        
        while( gracz.getSegmenty().contains(pozycja) && czyPrzeciwnik && przeciwnik.getSegmenty().contains(pozycja) ) {
            pozycja = new Point(r.nextInt(39), r.nextInt(29));
        }
        
        jablko.setPozycja(pozycja);
    }

    
}
